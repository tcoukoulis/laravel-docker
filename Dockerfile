FROM php:7.4.6-cli

RUN apt-get update && \
    apt-get install -y --no-install-recommends wget=1.20.1-1.1 \
        libzip-dev=1.5.1-4 \
        unzip=6.0-23+deb10u1 && \
    rm -rf /var/lib/apt/lists/* && \
    EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)" && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")" && \
    if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; then \
        >&2 echo ERROR: Invalid installer checksum && \
	rm composer-setup.php && exit 1; \
    fi && \
    php composer-setup.php && \
    mv composer.phar /usr/local/bin/composer && \
    docker-php-ext-install zip && \
    composer global require laravel/installer && \
    mkdir -p /home/workspace

WORKDIR /home/workspace

ENV PATH="/root/.composer/vendor/bin:${PATH}"
