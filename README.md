# Laravel Docker

## Purpose

I want to use the Laravel PHP package like a locally installed binary without actually installing or configuring PHP.

## Usage

From the root directory of the project, build the image.

```
docker build -t laravel:0.0.1 .
```

Add a function to your shell configuration.

```
laravel () {
    docker run --rm -it --mount src="$(pwd)",target=/home/workspace,type=bind laravel:0.0.1 laravel "$@"
}
```

This will execute the laravel process inside the container with a temporary binding of your host current working directory to the container's `/home/workspace` directory.
